import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';

import { Observable } from 'rxjs';

import { TokenService } from '@platform-core-lib/services/token.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
	private environment: any;
	// private platformStore: Store<PlatformCoreState>,
	constructor (private injector: Injector, private tokenService: TokenService) {
		this.environment = injector.get('ENVIRONMENT', {});
	}

	intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		const whiteListed = this.isWhiteListed(req);

		if (!whiteListed) {
			const accessToken = this.tokenService.getAccessToken();

			if (accessToken) {
				const headers = req.headers.append('Authorization', 'Bearer ' + accessToken);
				const authReq = req.clone({ headers });
				return next.handle(authReq);
			}
		}

		return next.handle(req);
	}

	private isWhiteListed (req: HttpRequest<any>): boolean {
		const whiteListArr: { method: string; endpoint: string }[] = this.environment.api.authInterceptorConfig.whiteList;
		const result = whiteListArr.find((whiteListObj) => {
			const listMethod: string = whiteListObj.method;
			const listEndpoint: string = whiteListObj.endpoint;
			let reqPathname = new URL(req.url).pathname;
			// to help comparison, trim the trailing / if present
			reqPathname = reqPathname.endsWith('/') ? reqPathname.substring(0, reqPathname.lastIndexOf('/')) : reqPathname;

			if (
				(listMethod === '*' || listMethod.toLowerCase() === req.method.toLowerCase()) &&
				((listEndpoint.endsWith('*') && reqPathname.startsWith(listEndpoint.replace('*', ''))) || listEndpoint === reqPathname)
			) {
				return true;
			}
		});
		return !!result;
	}
}
