export interface LocalStorageEvent {
  key: string;
  newValue?: any;
  storageType: string;
}

export interface LocalStorageConfig {
  prefix?: string;
  storageType?: 'sessionStorage' | 'localStorage';
}
