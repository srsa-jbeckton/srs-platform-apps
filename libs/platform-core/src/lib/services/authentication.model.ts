export interface ILoginCredentials {
  username: string;
  password: string;
  tnc: boolean;
}

export interface IAccessTokenPayload {
  uid: number;
}

export interface IAuthenticatedUser {
  id: number;
  username: string;
  email: string;
  locale: string;
  firstname: string;
  surname: string;
  userType: string;
  roleid: number;
}

// TODO: we should coordinate with Rob and derive some common interfaces for API responses, then refactor these out of here into a common location
export interface IValidateCredentialsResponse extends IAPIResponse {
  access_token?: string;
  uid?: string;
  two_factor_status?: string;
}

export interface IAPIResponse {
  status?: number;
  title?: string;
  detail?: string;
  statusText?: string;
}

export interface IPlatformLoginResult {
  authenticatedUser?: IAuthenticatedUser;
  error?: any;
}

export interface ICheckIsAuthenticatedResult {
  accessToken?: string;
  error?: any;
}
