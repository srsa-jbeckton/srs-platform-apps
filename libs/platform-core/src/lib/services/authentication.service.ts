import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';

import { Observable, of, throwError } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

import {
  IAccessTokenPayload,
  IAuthenticatedUser,
  ICheckIsAuthenticatedResult,
  ILoginCredentials,
  IPlatformLoginResult,
  IValidateCredentialsResponse,
} from '@platform-core-lib/services/authentication.model';
import { TokenService } from '@platform-core-lib/services/token.service';

@Injectable()
export class AuthenticationService {
	private environment: any;

	constructor (injector: Injector, private httpClient: HttpClient, private tokenService: TokenService) {
		this.environment = injector.get('ENVIRONMENT', {});
	}

	/**
   * Send user login credentials to server for validation.
   *
   * @param {ILoginCredentials} credentials
   * @returns {Observable<IValidateCredentialsResponse>}
   * @memberof AuthenticationService
   */
	public validateCredentials (credentials: ILoginCredentials): Observable<IValidateCredentialsResponse> {
		const url = this.environment.api.host + '/login';
		return this.httpClient
			.post<IValidateCredentialsResponse>(url, credentials, {
				withCredentials: true,
			})
			.pipe(
				catchError((response) => {
					return throwError(response);
				}),
			);
	}

	/**
   * Send two factor code to server for validation
   *
   * @param {string} twoFaCode
   * @returns {Observable<boolean>}
   * @memberof AuthenticationService
   */
	public validateTwoFACode (twoFaCode: string): Observable<boolean> {
		const url = this.environment.api.host + '/twofa';
		const options = {
			params: new HttpParams().set('code', twoFaCode),
		};

		return this.httpClient.get(url, options).pipe(
			map((response) => {
				return true;
			}),
			catchError((response) => {
				return throwError(response);
			}),
		);
	}

	/**
   * Gets the user object from the server based on the access token payload.
   * If the token or it's payload are invalid then method returns an error.
   * If the http request returns an error then the method returns the error to the caller.
   *
   * @param {string} accessToken
   * @returns {Observable<IPlatformLoginResult>}
   * @memberof AuthenticationService
   */
	public getAuthenticatedUser (accessToken: string): Observable<IPlatformLoginResult> {
		return new Observable<IPlatformLoginResult>((subscriber) => {
			// can we parse it? if payload is undefined then the token was bad
			const payload: IAccessTokenPayload = this.tokenService.parseToken(accessToken);

			if (payload && payload.uid) {
				// get the user data for session state

				const url = this.environment.api.host + '/users/' + payload.uid;
				this.httpClient.get<IAuthenticatedUser>(url).subscribe(
					(user: IAuthenticatedUser) => {
						subscriber.next({ authenticatedUser: user });
					},
					(error: any) => {
						// TODO: log this error.
						subscriber.next({ error: error });
					},
				);
			}
			else {
				subscriber.next({
					error: {
						message: 'Unable to parse access token or unable to obtain payload from access token',
					},
				});
			}
		});
	}

	/**
   * Determines if the current app user is authenticated.
   * Checks for access token, if not token then user is not considered Authenticated.
   * If Token exists then the token and session is validated by the server, if token or session is not valid
   * then the user is not Authenticated.
   * Note that being Authenticated is not the same as being logged in.
   *
   * @returns {Observable<ICheckIsAuthenticatedResult>}
   * @memberof AuthenticationService
   */
	public isAuthenticated (): Observable<ICheckIsAuthenticatedResult> {
		return new Observable((observer) => {
			const tokenServiceToken = this.tokenService.getAccessToken();
			if (tokenServiceToken) {
				this.serverSessionCheck().pipe(take(1)).subscribe(
					(sessionCheckResponse) => {
						if (!sessionCheckResponse.result) {
							observer.next({
								accessToken: tokenServiceToken,
								error: sessionCheckResponse.error ? sessionCheckResponse.error : null,
							});
						}
						else {
							observer.next({ accessToken: tokenServiceToken });
						}
					},
					(error) => {
						observer.next({ accessToken: tokenServiceToken, error: error });
					},
				);
			}
			else {
				observer.next({ accessToken: undefined });
			}
		});
	}

	/**
   * Allows the server to validate the access token as well as determine if the user has a current session.
   * The Auth Interceptor ( platform-core/src/http/auth.interceptor.ts ) service will provide the token in the requst headers to the server.
   *
   * @private
   * @returns {Observable<{ result?: boolean; error?: any }>}
   * @memberof AuthenticationService
   */
	private serverSessionCheck (): Observable<{ result?: boolean; error?: any }> {
		const url = this.environment.api.host + '/sessions';
		return this.httpClient.get(url).pipe(
			map((response) => {
				return { result: !!response };
			}),
			catchError((error) => {
				return of({ result: false, error: error });
			}),
		);
	}
}
