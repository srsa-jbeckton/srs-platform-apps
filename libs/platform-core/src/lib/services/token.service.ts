import { Injectable } from '@angular/core';

import { take } from 'rxjs/operators';

import { Store } from '@ngrx/store';

import { AuthenticationState, PlatformCoreState } from '@platform-core-lib/+state';
import { IAccessTokenPayload } from '@platform-core-lib/services/authentication.model';
import { LocalStorageService } from '@platform-core-lib/services/local-storage.service';

const LOCAL_STORAGE_TOKEN_KEY = 'accessToken';

@Injectable()
export class TokenService {
	constructor (private localStorageService: LocalStorageService, private platformStore: Store<PlatformCoreState>) {}

	public persistToken (accessToken: string): boolean {
		const storeTokenResultSuccess = this.localStorageService.set(LOCAL_STORAGE_TOKEN_KEY, accessToken);
		return storeTokenResultSuccess;
	}

	public getAccessToken (): string {
		let authState: AuthenticationState;
		this.platformStore.pipe(take(1)).subscribe((s) => (authState = s.authentication));

		// There are 2 places that the access token can be stored depending on the current auth state
		// 1. AuthenticationState.accessToken : if the user is authenticated/logged in or authentication / 2fa validation are in progress.
		// 2. Browser Local Storage : When the app bootstraps there is no state yet but there could have been a token persisted from a previous session or tab.
		const token =
			authState && authState.accessToken ? authState.accessToken : this.localStorageService.get<string>(LOCAL_STORAGE_TOKEN_KEY);

		return token || undefined;
	}

	public parseToken (token: string): IAccessTokenPayload | undefined {
		try {
			const base64Url = token.split('.')[1];
			const base64 = base64Url.replace('-', '+').replace('_', '/');
			const base64json = window.atob(base64);
			return JSON.parse(base64json) as IAccessTokenPayload;
		} catch (e) {
			console.error(e);
			return undefined;
		}
	}
}
