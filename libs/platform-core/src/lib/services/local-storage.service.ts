import { Injectable, Injector } from '@angular/core';

import { LocalStorageConfig } from '@platform-core-lib/services/local-storage.model';

@Injectable()
export class LocalStorageService {
	public isSupported = false;

	private config: LocalStorageConfig = {
		prefix: 'srs-ls-',
		storageType: 'localStorage',
	};
	private webStorage: Storage;

	constructor (injector: Injector) {
		// config can be injected
		const injectedConfig: LocalStorageConfig = injector.get('LOCAL_STORAGE_SERVICE_CONFIG', null);
		if (injectedConfig) {
			Object.assign(this.config, injectedConfig);
		}
		this.isSupported = this.checkSupport();
	}

	public clearAll (regularExpression: string): boolean {
		// Empty strings result in catchall RegExp
		const prefixRegex = !!this.config.prefix ? new RegExp('^' + this.config.prefix) : new RegExp('');
		const testRegex = !!regularExpression ? new RegExp(regularExpression) : new RegExp('');

		if (!this.isSupported) {
			return false;
		}

		const prefixLength = this.config.prefix.length;

		for (const key in this.webStorage) {
			// Only remove items that are for this app and match the regular expression
			if (prefixRegex.test(key) && testRegex.test(key.substr(prefixLength))) {
				try {
					this.remove(key.substr(prefixLength));
				} catch (e) {
					return false;
				}
			}
		}
		return true;
	}

	public deriveKey (key: string): string {
		return `${this.config.prefix}${key}`;
	}

	public get<T> (key: string): T {
		if (!this.isSupported) {
			return null;
		}

		const item = this.webStorage ? this.webStorage.getItem(this.deriveKey(key)) : null;
		// FIXME: not a perfect solution, since a valid 'null' string can't be stored
		if (!item || item === 'null') {
			return null;
		}

		try {
			return JSON.parse(item);
		} catch (e) {
			return null;
		}
	}

	public getStorageType (): string {
		return this.config.storageType;
	}

	public keys (): Array<string> {
		if (!this.isSupported) {
			return [];
		}

		const prefixLength = this.config.prefix.length;
		const keys: Array<string> = [];
		for (const key in this.webStorage) {
			// Only return keys that are for this app
			if (key.substr(0, prefixLength) === this.config.prefix) {
				try {
					keys.push(key.substr(prefixLength));
				} catch (e) {
					return [];
				}
			}
		}
		return keys;
	}

	public length (): number {
		let count = 0;
		const storage = this.webStorage;
		for (let i = 0; i < storage.length; i++) {
			if (storage.key(i).indexOf(this.config.prefix) === 0) {
				count += 1;
			}
		}
		return count;
	}

	public remove (...keys: Array<string>): boolean {
		let result = true;
		keys.forEach((key: string) => {
			if (!this.isSupported) {
				result = false;
			}

			try {
				this.webStorage.removeItem(this.deriveKey(key));
			} catch (e) {
				result = false;
			}
		});
		return result;
	}

	public set (key: string, value: any): boolean {
		// Let's convert `undefined` values to `null` to get the value consistent
		if (value === undefined) {
			value = null;
		}
		else {
			value = JSON.stringify(value);
		}

		if (!this.isSupported) {
			return false;
		}

		try {
			if (this.webStorage) {
				this.webStorage.setItem(this.deriveKey(key), value);
			}
		} catch (e) {
			return false;
		}
		return true;
	}

	private checkSupport (): boolean {
		try {
			const supported = this.config.storageType in window && window[this.config.storageType] !== null;

			if (supported) {
				this.webStorage = window[this.config.storageType];

				// When Safari (OS X or iOS) is in private browsing mode, it
				// appears as though localStorage is available, but trying to
				// call .setItem throws an exception.
				//
				// "QUOTA_EXCEEDED_ERR: DOM Exception 22: An attempt was made
				// to add something to storage that exceeded the quota."
				const key = this.deriveKey(`__${Math.round(Math.random() * 1e7)}`);
				this.webStorage.setItem(key, '');
				this.webStorage.removeItem(key);
			}

			return supported;
		} catch (e) {
			return false;
		}
	}
}
