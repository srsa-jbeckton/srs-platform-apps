import { PlatformState } from './platform/platform.interfaces';
import { platformInitialState } from './platform/platform.reducer';
import { AuthenticationState } from './authentication/authentication.interfaces';
import { authenticationInitialState } from './authentication/authentication.reducer';

export interface PlatformCoreState {
  readonly platform: PlatformState;
  readonly authentication: AuthenticationState;
}

export const platformCoreInitialState: PlatformCoreState = {
  platform: platformInitialState,
  authentication: authenticationInitialState
};

// Platform Store Export
export * from './platform/platform.reducer';
export * from './platform/platform.interfaces';
export * from './platform/platform.actions';
export { PlatformStoreEffects } from './platform/platform.effects';

// Authentication Store Export
export * from './authentication/authentication.reducer';
export * from './authentication/authentication.interfaces';
export * from './authentication/authentication.actions';
export {
  AuthenticationStoreEffects
} from './authentication/authentication.effects';

import * as authSelectors from './authentication/authentication.selectors';
export const AuthenticationSelectors = authSelectors;
