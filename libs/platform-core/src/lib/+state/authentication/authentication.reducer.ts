import {
  AuthenticationStoreActions,
  AuthenticationStoreActionTypes,
} from '@platform-core-lib/+state/authentication/authentication.actions';
import { AuthenticationState } from '@platform-core-lib/+state/authentication/authentication.interfaces';

export const authenticationInitialState: AuthenticationState = {
	accessToken: null,
	currentUser: null,
};

export function authenticationStoreReducer (state = authenticationInitialState, action: AuthenticationStoreActions): AuthenticationState {
	switch (action.type) {
		case AuthenticationStoreActionTypes.ValidateCredentials: {
			return {
				...state,
				currentUser: null,
				validateCredentialsState: {
					loading: true,
					loaded: false,
					isValid: null,
					error: null,
					twoFactorRequired: null,
					accessToken: null,
				},
				twoFactorAuthState: null,
				platformLoginState: null,
			};
		}

		case AuthenticationStoreActionTypes.ValidateCredentialsFailed: {
			return {
				...state,
				validateCredentialsState: {
					loading: false,
					loaded: true,
					isValid: false,
					error: action.payload,
					twoFactorRequired: false,
				},
			};
		}

		case AuthenticationStoreActionTypes.ValidateCredentialsSuccess: {
			return {
				...state,
				accessToken: action.payload.access_token,
				validateCredentialsState: {
					loading: false,
					loaded: true,
					isValid: true,
					error: null,
					accessToken: action.payload.access_token,
					twoFactorRequired: action.payload.two_factor_status === 'required' ? true : false,
				},
			};
		}

		case AuthenticationStoreActionTypes.ValidateTwoFactorCode: {
			return {
				...state,
				twoFactorAuthState: {
					loading: true,
					loaded: false,
					error: null,
					isValid: null,
				},
			};
		}

		case AuthenticationStoreActionTypes.ValidateTwoFactorCodeSuccess: {
			return {
				...state,
				twoFactorAuthState: {
					loading: false,
					loaded: true,
					error: null,
					isValid: true,
					accessToken: state.validateCredentialsState.accessToken,
				},
			};
		}

		case AuthenticationStoreActionTypes.ValidateTwoFactorCodeFailed: {
			return {
				...state,
				twoFactorAuthState: {
					loading: false,
					loaded: true,
					error: action.payload,
					isValid: false,
				},
			};
		}

		case AuthenticationStoreActionTypes.DoPlatformLogin: {
			return {
				...state,
				platformLoginState: {
					loading: true,
					loaded: false,
					error: null,
				},
				twoFactorAuthState: null,
			};
		}

		case AuthenticationStoreActionTypes.DoPlatformLoginSuccess: {
			return {
				...state,
				accessToken: action.payload.accessToken,
				currentUser: action.payload.currentUser,
				platformLoginState: {
					loading: false,
					loaded: true,
				},
				validateCredentialsState: null,
			};
		}

		case AuthenticationStoreActionTypes.DoPlatformLoginFailed: {
			return {
				...state,
				platformLoginState: {
					loading: false,
					loaded: true,
					error: action.payload,
				},
				validateCredentialsState: null,
			};
		}

		case AuthenticationStoreActionTypes.DoPlatformLoginCompleted: {
			return {
				...state,
				platformLoginState: null,
				validateCredentialsState: null,
			};
		}

		case AuthenticationStoreActionTypes.DoPlatformLogout: {
			return {
				...state,
				accessToken: null,
				currentUser: null,
			};
		}

		case AuthenticationStoreActionTypes.CheckIsAuthenticated: {
			return {
				...state,
				checkIsAuthenticatedState: {
					loading: true,
					loaded: false,
					correlationId: action.payload.correlationId,
				},
			};
		}

		case AuthenticationStoreActionTypes.CheckIsAuthenticatedSuccess: {
			return {
				...state,
				checkIsAuthenticatedState: {
					loading: false,
					loaded: true,
					result: true,
					correlationId: action.payload.correlationId,
					accessToken: action.payload.accessToken,
				},
			};
		}

		case AuthenticationStoreActionTypes.CheckIsAuthenticatedFailed: {
			return {
				...state,
				accessToken: null,
				currentUser: null,
				checkIsAuthenticatedState: {
					loading: false,
					loaded: true,
					correlationId: action.payload.correlationId,
					accessToken: action.payload.accessToken,
					result: false,
					error: action.payload.error ? action.payload.error : null,
				},
			};
		}

		case AuthenticationStoreActionTypes.CheckIsAuthenticatedCompleted: {
			return {
				...state,
				checkIsAuthenticatedState: null,
				platformAutoLoginState: null,
			};
		}

		case AuthenticationStoreActionTypes.DoPlatformAutoLogin: {
			return {
				...state,
				platformAutoLoginState: {
					loading: true,
					loaded: false,
					error: null,
					accessToken: action.payload.accessToken,
					correlationId: action.payload.correlationId,
				},
				currentUser: null,
				accessToken: null,
			};
		}

		case AuthenticationStoreActionTypes.DoPlatformAutoLoginSuccess: {
			return {
				...state,
				platformAutoLoginState: {
					loading: false,
					loaded: true,
					error: null,
					accessToken: action.payload.accessToken,
					correlationId: action.payload.correlationId,
				},
				currentUser: action.payload.currentUser ? action.payload.currentUser : state.currentUser,
				accessToken: action.payload.accessToken ? action.payload.accessToken : state.accessToken,
			};
		}

		case AuthenticationStoreActionTypes.DoPlatformAutoLoginFailed: {
			return {
				...state,
				platformAutoLoginState: {
					loading: false,
					loaded: true,
					error: action.payload.error,
					accessToken: action.payload.accessToken,
					correlationId: action.payload.correlationId,
				},
			};
		}

		default:
			return state;
	}
}
