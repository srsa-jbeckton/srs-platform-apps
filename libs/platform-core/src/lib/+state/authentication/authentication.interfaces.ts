import { IAuthenticatedUser } from '@platform-core-lib/services/authentication.model';

export interface AuthenticationState {
  readonly accessToken: string | null;
  readonly currentUser: CurrentUserState | null;
  readonly validateCredentialsState?: ValidateCredentialsState | null;
  readonly twoFactorAuthState?: TwoFactorAuthState | null;
  readonly platformLoginState?: PlatformLoginState | null;
  readonly checkIsAuthenticatedState?: CheckIsAuthenticatedState | null;
  readonly platformAutoLoginState?: PlatformAutoLoginState | null;
}

export interface ValidateCredentialsState {
  loading: boolean; // credentials validation in progress
  loaded: boolean; // credentials validation completed
  isValid?: boolean | null;
  error?: any | null; // credentials validation request failed, here is the error
  twoFactorRequired?: boolean | null; // server says enforce 2fa
  accessToken?: string | null; // if success then server returns an access token
}

export interface TwoFactorAuthState {
  loading: boolean; // two factor code validation in progress
  loaded: boolean; // two factor code validation completed
  isValid?: boolean | null; // was two factor code valid?
  error?: any | null; // two factor code validation error response
  accessToken?: string | null; // access token is passthrough from validate credentials, this is sent to the endpoint for this operation
}

export interface PlatformLoginState {
  loading: boolean; // platform login in progress
  loaded: boolean; // platform login completed
  error?: any | null; // platform login error
}

export interface PlatformAutoLoginState {
  loading: boolean; // platform login in progress
  loaded: boolean; // platform login completed
  error?: any | null; // platform login error
  correlationId: string;
  accessToken?: string;
}

export interface CheckIsAuthenticatedState {
  loading: boolean; // in progress
  loaded: boolean; // completed
  correlationId: string;
  accessToken?: string;
  result?: boolean;
  error?: any | null; // error result
}

export interface CurrentUserState extends IAuthenticatedUser {
  placeholder?: string;
}

export interface DoPlatformLoginSuccessPayload {
  accessToken: string;
  currentUser: IAuthenticatedUser;
}

export interface CheckIsAuthenticatedActionPayload {
  correlationId: string;
  accessToken?: string;
}

export interface CheckIsAuthenticatedFailedActionPayload
  extends CheckIsAuthenticatedActionPayload {
  error?: any;
}

export interface DoPlatformAutoLoginActionPayload {
  correlationId: string;
  accessToken?: string;
}

export interface DoPlatformAutoLoginSuccessActionPayload
  extends DoPlatformAutoLoginActionPayload {
  currentUser?: IAuthenticatedUser;
}

export interface DoPlatformAutoLoginFailedActionPayload
  extends DoPlatformAutoLoginActionPayload {
  error?: any;
}
