import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, of } from 'rxjs';
import { catchError, debounceTime, map, mergeMap, take, tap } from 'rxjs/operators';

import { Actions, Effect } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';

import { PlatformCoreState } from '@platform-core-lib/+state';
import {
	AuthenticationStoreActionTypes,
	CheckIsAuthenticatedAction,
	CheckIsAuthenticatedCompletedAction,
	CheckIsAuthenticatedFailedAction,
	CheckIsAuthenticatedSuccessAction,
	DoPlatformAutoLoginAction,
	DoPlatformAutoLoginFailedAction,
	DoPlatformAutoLoginSuccessAction,
	DoPlatformLoginAction,
	DoPlatformLoginCompletedAction,
	DoPlatformLoginFailedAction,
	DoPlatformLoginSuccessAction,
	DoPlatformLogoutCompletedAction,
	ValidateCredentialsAction,
	ValidateCredentialsFailedAction,
	ValidateCredentialsSuccessAction,
	ValidateTwoFactorCodeAction,
	ValidateTwoFactorCodeFailedAction,
	ValidateTwoFactorCodeSuccessAction,
} from '@platform-core-lib/+state/authentication/authentication.actions';
import {
	AuthenticationState,
	CheckIsAuthenticatedActionPayload,
	CheckIsAuthenticatedFailedActionPayload,
	DoPlatformAutoLoginActionPayload,
	DoPlatformAutoLoginFailedActionPayload,
	DoPlatformAutoLoginSuccessActionPayload,
} from '@platform-core-lib/+state/authentication/authentication.interfaces';
import { ICheckIsAuthenticatedResult, IPlatformLoginResult } from '@platform-core-lib/services/authentication.model';
import { AuthenticationService } from '@platform-core-lib/services/authentication.service';
import { TokenService } from '@platform-core-lib/services/token.service';

@Injectable()
export class AuthenticationStoreEffects {
	private environment: any;

	@Effect()
	validateCredentials: Observable<Action> = this.actions$.ofType(AuthenticationStoreActionTypes.ValidateCredentials).pipe(
		debounceTime(500),
		map((action: ValidateCredentialsAction) => action.payload),
		mergeMap((payload) => {
			return this.authService.validateCredentials(payload).pipe(
				map((response) => {
					return new ValidateCredentialsSuccessAction(response);
				}),
				catchError((err) => {
					return of(new ValidateCredentialsFailedAction(err));
				}),
			);
		}),
	);

	@Effect()
	validateTwoFactorCode: Observable<Action> = this.actions$.ofType(AuthenticationStoreActionTypes.ValidateTwoFactorCode).pipe(
		map((action: ValidateTwoFactorCodeAction) => action.payload),
		mergeMap((payload) => {
			return this.authService.validateTwoFACode(payload).pipe(
				map((response) => {
					return new ValidateTwoFactorCodeSuccessAction();
				}),
				catchError((err) => {
					return of(new ValidateTwoFactorCodeFailedAction(err));
				}),
			);
		}),
	);

	@Effect()
	doPlatformLogin: Observable<Action> = this.actions$.ofType(AuthenticationStoreActionTypes.DoPlatformLogin).pipe(
		map((action: DoPlatformLoginAction) => action.payload),
		mergeMap((payload) => {
			return this.authService.getAuthenticatedUser(payload).pipe(
				map((result) => {
					if (result.authenticatedUser) {
						return new DoPlatformLoginSuccessAction({
							currentUser: result.authenticatedUser,
							accessToken: payload,
						});
					}
					else {
						return new DoPlatformLoginFailedAction(result.error);
					}
				}),
			);
		}),
	);

	@Effect()
	doPlatformLoginSuccess: Observable<Action> = this.actions$.ofType(AuthenticationStoreActionTypes.DoPlatformLoginSuccess).pipe(
		map((action: DoPlatformLoginSuccessAction) => action.payload),
		mergeMap((payload) => {
			this.tokenService.persistToken(payload.accessToken);
			return of(new DoPlatformLoginCompletedAction());
		}),
	);

	@Effect({ dispatch: false })
	doPlatformLoginFailed = this.actions$.ofType(AuthenticationStoreActionTypes.DoPlatformLoginFailed).pipe(
		tap(() => {
			this.router.navigate([ this.environment.security.routes.platformLoginFailedUnkRoute ]);
		}),
	);

	@Effect({ dispatch: false })
	doPlatformLoginCompleted = this.actions$.ofType(AuthenticationStoreActionTypes.DoPlatformLoginCompleted).pipe(
		tap(() => {
			this.router.navigate([ this.environment.security.routes.postLoginRoute ]);
		}),
	);

	@Effect()
	doPlatformLogout: Observable<Action> = this.actions$.ofType(AuthenticationStoreActionTypes.DoPlatformLogout).pipe(
		mergeMap(() => {
			return of(new DoPlatformLogoutCompletedAction());
		}),
	);

	@Effect({ dispatch: false })
	doPlatformLogoutCompleted: Observable<Action> = this.actions$.ofType(AuthenticationStoreActionTypes.DoPlatformLogoutCompleted).pipe(
		tap(() => {
			this.router.navigate([ this.environment.security.routes.postLogoutRoute ]);
		}),
	);

	@Effect()
	checkIsAuthenticated: Observable<Action> = this.actions$.ofType(AuthenticationStoreActionTypes.CheckIsAuthenticated).pipe(
		map((action: CheckIsAuthenticatedAction) => action.payload),
		mergeMap((payload) => {
			// synchronously get the current authentication state from the store
			let authState: AuthenticationState;
			this.platformStore.pipe(take(1)).subscribe((s) => (authState = s.authentication)); // runs synchronously

			// determine is the user authenticated, checks for access token and valid session with server.
			return this.authService.isAuthenticated().pipe(
				take(1),
				map(
					(isAuthenticatedResult: ICheckIsAuthenticatedResult) => {
						// did isAuthenticated return an access token an no errors?
						if (!isAuthenticatedResult.error && isAuthenticatedResult.accessToken) {
							// the user is authenticated and has an active session.
							// is the user already logged in? has state?
							if (authState.currentUser && authState.accessToken) {
								// already logged in, we are done here.
								return new CheckIsAuthenticatedSuccessAction({
									correlationId: payload.correlationId,
									accessToken: isAuthenticatedResult.accessToken,
								});
							}
							else {
								// not logged in, has no state.
								// use auto login to log them in since we have a valid token.
								return new DoPlatformAutoLoginAction({
									correlationId: payload.correlationId,
									accessToken: isAuthenticatedResult.accessToken,
								});
							}
						}
						else {
							// no valid access token or server session
							// CheckIsAuthenticatedFailedAction reducer will destroy the currentUser and accessToken state
							return new CheckIsAuthenticatedFailedAction({
								correlationId: payload.correlationId,
								accessToken: payload.accessToken,
								error: isAuthenticatedResult.error,
							});
						}
					},
					(error) => {
						return new CheckIsAuthenticatedFailedAction({
							correlationId: payload.correlationId,
							accessToken: payload.accessToken,
							error: error,
						});
					},
				),
			);
		}),
	);

	@Effect()
	checkIsAuthenticatedSuccess: Observable<Action> = this.actions$.ofType(AuthenticationStoreActionTypes.CheckIsAuthenticatedSuccess).pipe(
		map((action: CheckIsAuthenticatedSuccessAction) => action.payload),
		mergeMap((payload: CheckIsAuthenticatedActionPayload) => {
			return of(new CheckIsAuthenticatedCompletedAction());
		}),
	);

	@Effect()
	checkIsAuthenticatedFailed: Observable<Action> = this.actions$.ofType(AuthenticationStoreActionTypes.CheckIsAuthenticatedFailed).pipe(
		map((action: CheckIsAuthenticatedFailedAction) => action.payload),
		mergeMap((payload: CheckIsAuthenticatedFailedActionPayload) => {
			return of(new CheckIsAuthenticatedCompletedAction());
		}),
	);

	@Effect()
	doPlatformAutoLogin: Observable<Action> = this.actions$.ofType(AuthenticationStoreActionTypes.DoPlatformAutoLogin).pipe(
		map((action: DoPlatformAutoLoginAction) => action.payload),
		mergeMap((payload: DoPlatformAutoLoginActionPayload) => {
			return this.authService.getAuthenticatedUser(payload.accessToken).pipe(
				take(1),
				map(
					(loginResult: IPlatformLoginResult) => {
						if (loginResult.authenticatedUser) {
							return new DoPlatformAutoLoginSuccessAction({
								correlationId: payload.correlationId,
								currentUser: loginResult.authenticatedUser,
								accessToken: payload.accessToken,
							});
						}
						else {
							// login failed
							return new DoPlatformAutoLoginFailedAction({
								correlationId: payload.correlationId,
								accessToken: payload.accessToken,
								error: loginResult.error,
							});
						}
					},
					(error: IPlatformLoginResult) => {
						return new DoPlatformAutoLoginFailedAction({
							correlationId: payload.correlationId,
							accessToken: payload.accessToken,
							error: error,
						});
					},
				),
			);
		}),
	);

	@Effect()
	doPlatformAutoLoginSuccess: Observable<Action> = this.actions$.ofType(AuthenticationStoreActionTypes.DoPlatformAutoLoginSuccess).pipe(
		map((action: DoPlatformAutoLoginSuccessAction) => action.payload),
		mergeMap((payload: DoPlatformAutoLoginSuccessActionPayload) => {
			return of(
				new CheckIsAuthenticatedSuccessAction({
					correlationId: payload.correlationId,
					accessToken: payload.accessToken,
				}),
			);
		}),
	);

	@Effect()
	doPlatformAutoLoginFailed: Observable<Action> = this.actions$.ofType(AuthenticationStoreActionTypes.DoPlatformAutoLoginFailed).pipe(
		map((action: DoPlatformAutoLoginFailedAction) => action.payload),
		mergeMap((payload: DoPlatformAutoLoginFailedActionPayload) => {
			return of(
				new CheckIsAuthenticatedFailedAction({
					correlationId: payload.correlationId,
					accessToken: payload.accessToken,
					error: payload.error,
				}),
			);
		}),
	);

	constructor (
		injector: Injector,
		private actions$: Actions,
		private authService: AuthenticationService,
		private router: Router,
		private platformStore: Store<PlatformCoreState>,
		private tokenService: TokenService,
	) {
		this.environment = injector.get('ENVIRONMENT', {});
	}
}
