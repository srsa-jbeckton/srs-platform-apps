import { createSelector } from '@ngrx/store';

import { PlatformCoreState } from '@platform-core-lib/+state';
import {
  AuthenticationState,
  CheckIsAuthenticatedState,
  PlatformLoginState,
  TwoFactorAuthState,
  ValidateCredentialsState,
} from '@platform-core-lib/+state/authentication/authentication.interfaces';

export const selectAuthenticationState = (state: PlatformCoreState) => {
	return state.authentication;
};

export const selectAccessToken = createSelector(selectAuthenticationState, (state: AuthenticationState) => state.accessToken);

export const selectValidateCredentialsState = createSelector(
	selectAuthenticationState,
	(state: AuthenticationState) => state.validateCredentialsState,
);
export const selectValidateCredentialsResult = createSelector(selectValidateCredentialsState, (state: ValidateCredentialsState) => {
	if (state && !state.loading && state.loaded && state.isValid != null) {
		return state;
	}
});

export const selectTwoFactorAuthState = createSelector(selectAuthenticationState, (state: AuthenticationState) => state.twoFactorAuthState);
export const selectTwoFactorAuthResult = createSelector(selectTwoFactorAuthState, (state: TwoFactorAuthState) => {
	if (state && !state.loading && state.loaded && state.isValid != null) {
		return state;
	}
});

export const selectPlatformLoginState = createSelector(selectAuthenticationState, (state: AuthenticationState) => state.platformLoginState);
export const selectPlatformLoginResult = createSelector(selectPlatformLoginState, (state: PlatformLoginState) => {
	if (state && !state.loading && state.loaded) {
		return state;
	}
});

export const selectCheckIsAuthenticatedState = createSelector(
	selectAuthenticationState,
	(state: AuthenticationState) => state.checkIsAuthenticatedState,
);
export const selectCheckIsAuthenticatedResult = createSelector(selectCheckIsAuthenticatedState, (state: CheckIsAuthenticatedState) => {
	if (state && !state.loading && state.loaded) {
		return state;
	}
});

export const selectCurrentUser = createSelector(selectAuthenticationState, (state: AuthenticationState) => {
	return state.currentUser;
});

export const selectIsLoggedIn = createSelector(selectAuthenticationState, (state: AuthenticationState) => {
	return !!(state.currentUser && state.accessToken);
});
