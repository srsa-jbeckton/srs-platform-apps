import { Action } from '@ngrx/store';

import {
  CheckIsAuthenticatedActionPayload,
  CheckIsAuthenticatedFailedActionPayload,
  DoPlatformAutoLoginActionPayload,
  DoPlatformAutoLoginFailedActionPayload,
  DoPlatformAutoLoginSuccessActionPayload,
  DoPlatformLoginSuccessPayload,
} from '@platform-core-lib/+state/authentication/authentication.interfaces';
import {
  IAPIResponse,
  ILoginCredentials,
  IValidateCredentialsResponse,
} from '@platform-core-lib/services/authentication.model';

export enum AuthenticationStoreActionTypes {
	SetAccessToken = '[Authentication] Set Access Token',
	ValidateCredentials = '[Authentication] Validate Credentials',
	ValidateCredentialsSuccess = '[Authentication] Validate Credentials Success',
	ValidateCredentialsFailed = '[Authentication] Validate Credentials Failed',
	ValidateTwoFactorCode = '[Authentication] Validate Two Factor Code',
	ValidateTwoFactorCodeSuccess = '[Authentication] Validate Two Factor Code Success',
	ValidateTwoFactorCodeFailed = '[Authentication] Validate Two Factor Code Failed',
	DoPlatformLogin = '[Authentication] Do Platform Login',
	DoPlatformLoginSuccess = '[Authentication] Do Platform Login Success',
	DoPlatformLoginFailed = '[Authentication] Do Platform Login Failed',
	DoPlatformLoginCompleted = '[Authentication] Do Platform Login Completed',
	DoPlatformLogout = '[Authentication] Do Platform Logout',
	DoPlatformLogoutCompleted = '[Authentication] Do Platform Logout Completed',
	CheckIsAuthenticated = '[Authentication] Check is Authenticated',
	CheckIsAuthenticatedSuccess = '[Authentication] Check is Authenticated Success',
	CheckIsAuthenticatedFailed = '[Authentication] Check is Authenticated Failed',
	CheckIsAuthenticatedCompleted = '[Authentication] Check is Authenticated Complete',
	DoPlatformAutoLogin = '[Authentication] Do Platform Auto Login',
	DoPlatformAutoLoginSuccess = '[Authentication] Do Platform Auto Login Success',
	DoPlatformAutoLoginFailed = '[Authentication] Do Platform Auto Login Failed',
}

export class SetAccessTokenAction implements Action {
	readonly type = AuthenticationStoreActionTypes.SetAccessToken;
	constructor (public payload: string) {}
}

export class ValidateCredentialsAction implements Action {
	readonly type = AuthenticationStoreActionTypes.ValidateCredentials;
	constructor (public payload: ILoginCredentials) {}
}

export class ValidateCredentialsSuccessAction implements Action {
	readonly type = AuthenticationStoreActionTypes.ValidateCredentialsSuccess;
	constructor (public payload: IValidateCredentialsResponse) {}
}

export class ValidateCredentialsFailedAction implements Action {
	readonly type = AuthenticationStoreActionTypes.ValidateCredentialsFailed;
	constructor (public payload: IAPIResponse) {}
}

export class ValidateTwoFactorCodeAction implements Action {
	readonly type = AuthenticationStoreActionTypes.ValidateTwoFactorCode;
	constructor (public payload: string) {}
}

export class ValidateTwoFactorCodeSuccessAction implements Action {
	readonly type = AuthenticationStoreActionTypes.ValidateTwoFactorCodeSuccess;
	constructor () {}
}

export class ValidateTwoFactorCodeFailedAction implements Action {
	readonly type = AuthenticationStoreActionTypes.ValidateTwoFactorCodeFailed;
	constructor (public payload: any) {}
}

export class DoPlatformLoginAction implements Action {
	readonly type = AuthenticationStoreActionTypes.DoPlatformLogin;
	constructor (public payload: string) {}
}

export class DoPlatformLoginSuccessAction implements Action {
	readonly type = AuthenticationStoreActionTypes.DoPlatformLoginSuccess;
	constructor (public payload: DoPlatformLoginSuccessPayload) {}
}
export class DoPlatformLoginFailedAction implements Action {
	readonly type = AuthenticationStoreActionTypes.DoPlatformLoginFailed;
	constructor (public payload: any) {}
}

export class DoPlatformLoginCompletedAction implements Action {
	readonly type = AuthenticationStoreActionTypes.DoPlatformLoginCompleted;
	//constructor(public payload: any) { }
}

export class DoPlatformLogoutAction implements Action {
	readonly type = AuthenticationStoreActionTypes.DoPlatformLogout;
}

export class DoPlatformLogoutCompletedAction implements Action {
	readonly type = AuthenticationStoreActionTypes.DoPlatformLogoutCompleted;
}
export class CheckIsAuthenticatedAction implements Action {
	readonly type = AuthenticationStoreActionTypes.CheckIsAuthenticated;
	constructor (public payload: CheckIsAuthenticatedActionPayload) {}
}

export class CheckIsAuthenticatedSuccessAction implements Action {
	readonly type = AuthenticationStoreActionTypes.CheckIsAuthenticatedSuccess;
	constructor (public payload: CheckIsAuthenticatedActionPayload) {}
}

export class CheckIsAuthenticatedFailedAction implements Action {
	readonly type = AuthenticationStoreActionTypes.CheckIsAuthenticatedFailed;
	constructor (public payload: CheckIsAuthenticatedFailedActionPayload) {}
}

export class CheckIsAuthenticatedCompletedAction implements Action {
	readonly type = AuthenticationStoreActionTypes.CheckIsAuthenticatedCompleted;
	//constructor(public payload: CheckIsAuthenticatedCompletedActionPayload) {}
}

export class DoPlatformAutoLoginAction implements Action {
	readonly type = AuthenticationStoreActionTypes.DoPlatformAutoLogin;
	constructor (public payload: DoPlatformAutoLoginActionPayload) {}
}

export class DoPlatformAutoLoginSuccessAction implements Action {
	readonly type = AuthenticationStoreActionTypes.DoPlatformAutoLoginSuccess;
	constructor (public payload: DoPlatformAutoLoginSuccessActionPayload) {}
}

export class DoPlatformAutoLoginFailedAction implements Action {
	readonly type = AuthenticationStoreActionTypes.DoPlatformAutoLoginFailed;
	constructor (public payload: DoPlatformAutoLoginFailedActionPayload) {}
}

export type AuthenticationStoreActions =
	| SetAccessTokenAction
	| ValidateCredentialsAction
	| ValidateCredentialsSuccessAction
	| ValidateCredentialsFailedAction
	| ValidateTwoFactorCodeAction
	| ValidateTwoFactorCodeSuccessAction
	| ValidateTwoFactorCodeFailedAction
	| DoPlatformLoginAction
	| DoPlatformLoginSuccessAction
	| DoPlatformLoginFailedAction
	| DoPlatformLoginCompletedAction
	| DoPlatformLogoutAction
	| DoPlatformLogoutCompletedAction
	| CheckIsAuthenticatedAction
	| CheckIsAuthenticatedSuccessAction
	| CheckIsAuthenticatedFailedAction
	| CheckIsAuthenticatedCompletedAction
	| DoPlatformAutoLoginAction
	| DoPlatformAutoLoginSuccessAction
	| DoPlatformAutoLoginFailedAction;
