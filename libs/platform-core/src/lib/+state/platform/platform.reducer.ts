import { PlatformStoreActions, PlatformStoreActionTypes } from '@platform-core-lib/+state/platform/platform.actions';
import { PlatformState } from '@platform-core-lib/+state/platform/platform.interfaces';

export const platformInitialState: PlatformState = {
	title: 'SRS Platform Core',
};

export function platformStoreReducer (state = platformInitialState, action: PlatformStoreActions): PlatformState {
	switch (action.type) {
		case PlatformStoreActionTypes.PlatformAction:
			return state;

		case PlatformStoreActionTypes.PlatformLoaded: {
			return { ...state, ...action.payload };
		}

		default:
			return state;
	}
}
