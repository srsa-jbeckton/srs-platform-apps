import { Action } from '@ngrx/store';

export enum PlatformStoreActionTypes {
	PlatformAction = '[Platform Core] Action',
	LoadPlatform = '[Platform Core] Load Data',
	PlatformLoaded = '[Platform Core] Data Loaded',
	PlatformLogin = '[Platform Core] Platform Login',
}

export class PlatformAction implements Action {
	readonly type = PlatformStoreActionTypes.PlatformAction;
}

export class LoadPlatformAction implements Action {
	readonly type = PlatformStoreActionTypes.LoadPlatform;
	constructor (public payload: any) {}
}

export class PlatformLoadedAction implements Action {
	readonly type = PlatformStoreActionTypes.PlatformLoaded;
	constructor (public payload: any) {}
}

export class PlatformLoginAction implements Action {
	readonly type = PlatformStoreActionTypes.PlatformLoaded;
	constructor (public payload: string) {}
}

export type PlatformStoreActions = PlatformAction | LoadPlatformAction | PlatformLoadedAction;
