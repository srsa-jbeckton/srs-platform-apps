import { Injectable } from '@angular/core';

import { Actions, Effect } from '@ngrx/effects';

import { DataPersistence } from '@nrwl/nx';

import {
  LoadPlatformAction,
  PlatformLoadedAction,
  PlatformStoreActionTypes,
} from '@platform-core-lib/+state/platform/platform.actions';
import { PlatformState } from '@platform-core-lib/+state/platform/platform.interfaces';

@Injectable()
export class PlatformStoreEffects {
	@Effect() effect$ = this.actions$.ofType(PlatformStoreActionTypes.PlatformAction);

	@Effect()
	loadPlatform$ = this.dataPersistence.fetch(PlatformStoreActionTypes.LoadPlatform, {
		run: (action: LoadPlatformAction, state: PlatformState) => {
			return new PlatformLoadedAction(state);
		},

		onError: (action: LoadPlatformAction, error) => {
			console.error('Error', error);
		},
	});

	constructor (private actions$: Actions, private dataPersistence: DataPersistence<PlatformState>) {}
}
