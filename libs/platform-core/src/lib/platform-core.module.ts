import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';

import { AuthenticationGuard } from '@platform-core-lib/guards/authentication.guard';
import { AuthInterceptor } from '@platform-core-lib/http/auth.interceptor';
import { throwIfAlreadyLoaded } from '@platform-core-lib/module-import-guard';
import { AuthenticationService } from '@platform-core-lib/services/authentication.service';
import { LocalStorageService } from '@platform-core-lib/services/local-storage.service';
import { TokenService } from '@platform-core-lib/services/token.service';

@NgModule({
	imports: [ CommonModule ],
	providers: [],
})
export class PlatformCoreModule {
	constructor (
		@Optional()
		@SkipSelf()
		parentModule: PlatformCoreModule,
	) {
		throwIfAlreadyLoaded(parentModule, 'CoreModule');
	}

	static forRoot (): ModuleWithProviders {
		return {
			ngModule: PlatformCoreModule,
			providers: [
				AuthenticationService,
				TokenService,
				LocalStorageService,
				AuthInterceptor,
				AuthenticationGuard,
				{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
			],
		};
	}
}
