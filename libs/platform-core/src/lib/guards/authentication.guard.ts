import { Injectable, Injector } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, CanLoad, Route, Router, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { Store } from '@ngrx/store';

import { PlatformCoreState } from '@platform-core-lib/+state';
import { AuthenticationSelectors, CheckIsAuthenticatedAction, CheckIsAuthenticatedState } from '@platform-core-lib/+state/';

@Injectable()
export class AuthenticationGuard implements CanActivateChild, CanActivate, CanLoad {
	private environment: any;

	constructor (protected router: Router, injector: Injector, private store: Store<PlatformCoreState>) {
		this.environment = injector.get('ENVIRONMENT', {});
	}

	canActivate (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		return this.checkIsAuthenticated();
	}

	canActivateChild (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		return this.checkIsAuthenticated();
	}

	canLoad (route: Route): Observable<boolean> {
		return this.checkIsAuthenticated();
	}

	public checkIsAuthenticated (): Observable<boolean> {
		return new Observable((observer) => {
			const correlationId = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
			this.store
				.select(AuthenticationSelectors.selectCheckIsAuthenticatedResult)
				.pipe(
					filter((r) => {
						if (r && r.correlationId) {
							return r.correlationId === correlationId;
						}
					}),
					first(),
				)
				.subscribe((state: CheckIsAuthenticatedState) => {
					if (state.result) {
						observer.next(true);
					}
					else {
						observer.next(false);
						this.router.navigate([ this.environment.security.routes.loginComponentRoute ]);
					}
					observer.complete();
				});

			this.store.dispatch(new CheckIsAuthenticatedAction({ correlationId: correlationId }));
		});
	}
}
