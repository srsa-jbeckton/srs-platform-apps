import { ICoreEnvironment } from './environment.interfaces';

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular.json`.

export const environment: ICoreEnvironment = {
	production: false,
	api: {
		host: 'https://staging.api.server27.srsacquiom.com',
		authInterceptorConfig: {
			// send Authorization header for all API calls EXCEPT for the whitelisted API endpoints.
			// NOTE: do not end endpoint string with a trailing slash '/'. example avoid '/sample/'.
			// NOTE: use an asteric at the END if you want to whitelist endpoint paths that start with.
			// example '/users*' will whitelist all paths that start with '/users' like '/users/1233345' or '/users/12345/foo'.

			whiteList: [
				{ method: '*', endpoint: '/login*' },
				{ method: 'get', endpoint: '/sample' },
				{ method: '*', endpoint: '/another/sample*' },
			],
		},
	},
	security: {
		routes: {
			loginComponentRoute: '/security/login', // login flow entry, this is where the user goes to login
			postLoginRoute: '/', // after successful login
			postLogoutRoute: '/security/login', // after successful logout
			platformLoginFailedUnkRoute: '/security/login', // this is not for invalid credentials, this is for when the login routine fails for an unknown reason.
		},
		sessionConfig: {
			idleSeconds: 300, // 300 inactivity timeout
			timeoutSeconds: 30, // 30 after idleSeconds, the users session will timeout after timeoutSeconds
			keepAlivePingSeconds: 300, // 300 interval to ping the server to keep server side session alive
		},
	},
	analytics: {
		debug: true,
		google: {
			config: {
				id: 'UA-113883546-3',
			},
		},
	},
};
