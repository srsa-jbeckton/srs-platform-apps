export interface ICoreEnvironment {
	production: boolean;
	api: IApi;
	security: any;
	analytics: any;
}

export interface IApi {
	host: string;
	authInterceptorConfig: IAuthInterceptorConfig;
}

export interface IAuthInterceptorConfig {
	whiteList: Array<{ method: string; endpoint: string }>;
}

// TODO: finish these interfaces
