export { PlatformCoreModule } from './lib/platform-core.module';
export { AuthenticationService } from './lib/services/authentication.service';
export * from './lib/services/authentication.model';
export { TokenService } from './lib/services/token.service';
export * from './lib/services/local-storage.model';
export { AuthenticationGuard } from './lib/guards/authentication.guard';

export * from './lib/+state';

export { ICoreEnvironment } from './environments/environment.interfaces';
export { environment } from './environments/environment';
