export interface ILoginCredentials {
  username: string;
  password: string;
  tnc: boolean;
}
