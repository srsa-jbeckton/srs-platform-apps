import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Store } from '@ngrx/store';

import {
  AuthenticationSelectors,
  DoPlatformLoginAction,
  PlatformCoreState,
  PlatformLoginState,
  ValidateCredentialsAction,
  ValidateCredentialsState,
} from '@srs-platform/platform-core';

@Component({
	selector: 'app-login',
	template: `<div>Click the button to login</div>
  <button (click)="login()">Login</button>
  <p>{{message}}</p>`,
})
export class LoginComponent implements OnInit, OnDestroy {
	private unsubscribeSubject: Subject<boolean> = new Subject<boolean>();
	public message: string;

	constructor (private store: Store<PlatformCoreState>, private router: Router) {}

	ngOnInit () {
		// TODO: handle platform login failed state
		this.store
			.select(AuthenticationSelectors.selectPlatformLoginResult)
			.pipe(takeUntil(this.unsubscribeSubject))
			.subscribe((state: PlatformLoginState) => {
				if (state && state.error) {
					// TODO: show error message that the platform login operation failed for an unknown reason.
					console.log(state.error);
				}
			});

		this.store
			.select(AuthenticationSelectors.selectValidateCredentialsResult)
			.pipe(takeUntil(this.unsubscribeSubject))
			.subscribe((state: ValidateCredentialsState) => {
				if (state) {
					if (state.isValid) {
						// success
						if (state.twoFactorRequired) {
							// navigate to two fa form
							this.router.navigate([ '/security/validate-code' ]);
						}
						else {
							// dispatch platform login
							this.store.dispatch(new DoPlatformLoginAction(state.accessToken));
						}
					}
					else {
						// failed
						// TODO: display error message
					}
				}
			});
	}

	ngOnDestroy () {
		this.unsubscribeSubject.next(false);
	}

	public login (): void {
		this.store.dispatch(
			new ValidateCredentialsAction({
				username: 'testv1',
				password: 'password',
				tnc: true,
			}),
		);
	}
}
