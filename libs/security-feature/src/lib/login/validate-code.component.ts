import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subject } from 'rxjs';

import { Store } from '@ngrx/store';

import {
  AuthenticationSelectors,
  AuthenticationService,
  DoPlatformLoginAction,
  PlatformCoreState,
  TwoFactorAuthState,
  ValidateTwoFactorCodeAction,
} from '@srs-platform/platform-core';

@Component({
	selector: 'app-validate-code',
	template: `<h2>2fa works</h2>
  <button (click)="go()">Validate 2FA</button>`,
})
export class ValidateCodeComponent implements OnInit, OnDestroy {
	private unsubscribeSubject: Subject<boolean> = new Subject<boolean>();

	constructor (private store: Store<PlatformCoreState>, private authService: AuthenticationService) {}

	ngOnInit () {
		this.store.select(AuthenticationSelectors.selectTwoFactorAuthResult).subscribe((state: TwoFactorAuthState) => {
			if (state) {
				if (state.isValid) {
					// success
					this.store.dispatch(new DoPlatformLoginAction(state.accessToken));
				}
				else {
					// failed
					// TODO: display error message
				}
			}
		});
	}

	ngOnDestroy () {
		this.unsubscribeSubject.next(false);
	}

	go (): void {
		this.store.dispatch(new ValidateTwoFactorCodeAction('8675309'));
	}
}
