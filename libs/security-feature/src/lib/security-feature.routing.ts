import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from '@security-feature-lib/login/login.component';
import { ValidateCodeComponent } from '@security-feature-lib/login/validate-code.component';
import { SecurityFeatureComponent } from '@security-feature-lib/security-feature.component';

const routes: Routes = [
	{
		path: '',
		component: SecurityFeatureComponent, // layout template for security
		children: [
			{ path: '', redirectTo: 'login', pathMatch: 'full' },
			{
				path: 'login',
				component: LoginComponent,
			},
			{ path: 'validate-code', component: ValidateCodeComponent },
		],
	},
];

@NgModule({
	imports: [ RouterModule.forChild(routes) ],
	exports: [ RouterModule ],
})
export class SecurityFeatureRoutingModule {}
