import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LoginComponent } from '@security-feature-lib/login/login.component';
import { ValidateCodeComponent } from '@security-feature-lib/login/validate-code.component';
import { SecurityFeatureComponent } from '@security-feature-lib/security-feature.component';
import { SecurityFeatureRoutingModule } from '@security-feature-lib/security-feature.routing';

@NgModule({
	imports: [ CommonModule, SecurityFeatureRoutingModule ],
	declarations: [ SecurityFeatureComponent, LoginComponent, ValidateCodeComponent ],
	providers: [],
})
export class SecurityFeatureModule {}
