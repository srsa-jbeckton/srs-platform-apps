import { environment as coreEnvironment, ICoreEnvironment } from '@srs-platform/platform-core';

export interface IAppEnvironment extends ICoreEnvironment {
	app?: any;
}

export const environment: IAppEnvironment = {
	...coreEnvironment,
	analytics: {
		...coreEnvironment.analytics,
		portalType: 'APP SEED',
	},
};
