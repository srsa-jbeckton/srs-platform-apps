import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  template: `<h2>Dashboard Works!</h2>
        <a routerLink="/sample">Go To Sample</a>
    `
})
export class DashboardComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
