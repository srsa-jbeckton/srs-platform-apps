import { AppActions, AppActionTypes } from './app.actions';

/**
 * Interface for the 'App' data used in
 *  - AppState, and
 *  - appReducer
 */
export interface AppData {
	title: string;
}

/**
 * Interface to the part of the Store containing AppState
 * and other information related to AppData.
 */
export interface AppState {
	readonly app: AppData;
}

export const initialState: AppData = {
	title: 'Buyer Portal',
};

export function appReducer (state = initialState, action: AppActions): AppData {
	switch (action.type) {
		case AppActionTypes.AppAction:
			return state;

		case AppActionTypes.AppLoaded: {
			return { ...state, ...action.payload };
		}

		default:
			return state;
	}
}

export const getRootState = (state: AppData) => state;
