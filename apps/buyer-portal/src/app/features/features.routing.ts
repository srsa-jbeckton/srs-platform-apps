import { NgModule } from '@angular/core';
import { NoPreloading, RouterModule, Routes } from '@angular/router';

import { AuthenticationGuard } from '@srs-platform/platform-core';

import { DashboardComponent } from '@apps/admin-portal/features/dashboard/dashboard.component';
import { FeaturesComponent } from '@apps/admin-portal/features/features.component';
import { SampleComponent } from '@apps/admin-portal/features/sample/sample.component';

const routes: Routes = [
	{
		path: '',
		component: FeaturesComponent, // layout template for authenticated features
		children: [
			{ path: '', redirectTo: 'dashboard', pathMatch: 'full' },
			{
				path: 'dashboard',
				canActivate: [ AuthenticationGuard ],
				component: DashboardComponent,
			},
			{
				path: 'sample',
				canActivate: [ AuthenticationGuard ],
				component: SampleComponent,
			},
			// add feature routes here
		],
	},
	{
		path: 'security',
		loadChildren: '@srs-platform/security-feature#SecurityFeatureModule',
	},
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, {
			preloadingStrategy: NoPreloading,
			useHash: false,
			initialNavigation: 'enabled',
		}),
	],
	exports: [ RouterModule ],
})
export class FeaturesRoutingModule {}
