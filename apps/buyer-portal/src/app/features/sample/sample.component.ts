import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sample',
  template: `<h2>Sample Works!</h2>
	<a routerLink="/dashboard">Go To Dashboard</a>`
})
export class SampleComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
