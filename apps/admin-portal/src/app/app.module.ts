import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { EffectsModule } from '@ngrx/effects';
import { RouterStateSerializer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { NxModule } from '@nrwl/nx';

import { environment } from '../environments/environment';
import { storeFreeze } from 'ngrx-store-freeze';

import {
	AuthenticationStoreEffects,
	authenticationStoreReducer,
	platformCoreInitialState,
	PlatformCoreModule,
	PlatformStoreEffects,
	platformStoreReducer,
} from '@srs-platform/platform-core';

import { AppEffects } from '@apps/admin-portal/+state/app.effects';
import { appReducer, initialState as appInitialState } from '@apps/admin-portal/+state/app.reducer';
import { CustomSerializer } from '@apps/admin-portal/+state/custom-serializer';
import { AppComponent } from '@apps/admin-portal/app.component';
import { FeaturesModule } from '@apps/admin-portal/features/features.module';

@NgModule({
	imports: [
		BrowserModule,
		RouterModule,
		FeaturesModule,
		NxModule.forRoot(),
		PlatformCoreModule.forRoot(),
		HttpClientModule,
		StoreModule.forRoot(
			{
				app: appReducer,
				platform: platformStoreReducer,
				authentication: authenticationStoreReducer,
			},
			{
				initialState: {
					app: appInitialState,
					platform: platformCoreInitialState.platform,
					authentication: platformCoreInitialState.authentication,
				},
				metaReducers: !environment.production ? [ storeFreeze ] : [],
			},
		),
		EffectsModule.forRoot([ AppEffects, PlatformStoreEffects, AuthenticationStoreEffects ]),
		!environment.production ? StoreDevtoolsModule.instrument() : [],
		StoreRouterConnectingModule,
	],
	declarations: [ AppComponent ],
	bootstrap: [ AppComponent ],
	providers: [ { provide: RouterStateSerializer, useClass: CustomSerializer }, { provide: 'ENVIRONMENT', useValue: environment } ],
})
export class AppModule {}
