import { NgModule } from '@angular/core';

import { DashboardComponent } from '@apps/admin-portal/features/dashboard/dashboard.component';
import { FeaturesComponent } from '@apps/admin-portal/features/features.component';
import { FeaturesRoutingModule } from '@apps/admin-portal/features/features.routing';
import { SampleComponent } from '@apps/admin-portal/features/sample/sample.component';

@NgModule({
	imports: [
		// SharedModule,
		FeaturesRoutingModule,
	],
	declarations: [ FeaturesComponent, DashboardComponent, SampleComponent ],
})
export class FeaturesModule {}
